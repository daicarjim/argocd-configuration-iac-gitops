# Configuration ARGOCD IaC GITOPS

If you want configure ArgoCD, the form recommended is with files .yaml because you can have a history the all configurations, for you and all your teams.

## How to use

## Requirements 📋


The next tutorial you should apply before:
```
https://gitlab.com/daicarjim/k3s-cluster-pro
```
 
Or you should have a cluster basic in hetzner cloud with LoadBalancer.

Also

```
https://gitlab.com/daicarjim/gitlab-server-with-terraform-in-hetznercloud
```


Additional configure ingress with files 001 and 002 of the repository:

```
https://gitlab.com/daicarjim/ingress-production-hetzner-with-loadbalancerk8s
```

You should create two repositories with files 003 and 004, 005 respectively.

## Clone this repository 📄

```

git clone https://gitlab.com/daicarjim/argocd-configuration-iac-gitops.git
```
_ATTENTION: You should download this repository in you master machine of cluster because you have configure some the featutes and apply manifests yaml._


_WARNING: You should create two repositories with files 003 and 004, 005 respectively. Then configured in the .yaml this repositories that you created._


1) Create variables enviroment in the cluster:

```
export USERNAME=your user git repository
```
```
export PASSWORD=your password git repository
```

_NOTE: The next variables are test for login registry.... still need to test the correct connection._

```
export URLDOCKERREGISTRY=url registry gitlab example: registry.gitlab.com
```
```
export USERNAMEDOCKER=username with privileges
```
```
export PASSWORDDOCKER=password username with privileges
```
```
export EMAILDOCKERREGISTRY=email username with privileges
```



2) Now you have that create the new file that replace the variables enviroment:

```
envsubst < secret-repository-argocd.yaml > namexampleargo.yaml
```

3) Then create secret with:

```
kubectl apply -f namexampleargo.yaml 
```

4) Now you have that create the new file that replace the variables enviroment:

_NOTE: Test... for the moment you can enable repository public and configure acess for only members_

```
envsubst < secret-registry-gitlab.yaml > namexampleregistry.yaml
```

5) Then create secret with:

```
kubectl apply -f namexampleregistry.yaml
```
  

6) Access and then change in file configuration-argocd.yaml the url repositories:


![alt text](imagen configuracion archivo.png)


_REMEMBER REPLACE ALL SPACE NECESARY FOR THE URLS IN THE YAML OF CONFIGURATION_

_NOTE: REMEMBER WRITE URL WITH EXTENSION .GIT IN THE END_

7) Save and apply changes in your cluster:

```
kubectl apply -f configuration-argocd.yaml 
```


# CONGRATULATION... Now you have an ingress with GITOPS in mode-production with Hetzner-Cloud and Load Balancer. 
# Now you can see your application in the domain and subdomains configure.


# GRATITUDE 🎁

```
https://www.adictosaltrabajo.com/2020/05/25/implementando-gitops-con-argocd/
https://medium.com/@andrew.kaczynski/gitops-in-kubernetes-argo-cd-and-gitlab-ci-cd-5828c8eb34d6
https://argoproj.github.io/argo-cd/operator-manual/declarative-setup/
https://kubernetes.io/es/docs/concepts/configuration/secret/
https://www.udemy.com/course/kubernetes-de-principiante-a-experto/
https://vix.digital/blog/technology/how-get-kubernetes-pulling-private-gitlab-container-registry/
```

⌨️ Esto se hizo con el ❤️ para la comunidad por [daicarjim](https://gitlab.com/daicarjim) 👍
